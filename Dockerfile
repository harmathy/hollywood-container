FROM debian:trixie-20230612

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /usr/share/man/man1 \
    && apt-get update \
    && apt-get -qy upgrade \
    && apt-get -qy dist-upgrade \
    && apt-get -qy install --install-recommends \
        hollywood \
        man \
    && apt-get -qy autoremove --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -m user

USER user
CMD ["hollywood"]
